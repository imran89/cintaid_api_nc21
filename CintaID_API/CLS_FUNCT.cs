﻿using CintaID_API.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace CintaID_API
{
    public class CLS_FUNCT
    {
        public static string cintaidkeys = "^Cinta.ID-L3xY*0MT*83@$T!3^";
        public static string conn = "Server=cintaid.database.windows.net;Database=B2b;User Id=thomas.benny;Password=Mcfurry.2011";
        public static string conn2 = "Server=foodieGO.database.windows.net;Database=CintaID;User Id=thomas.benny;Password=Mcfurry.2011";
        public static string URITransDom = "/banking/corporates/transfers/domestic";
        public static string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            string Temp;
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new System.Security.Cryptography.HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                Temp = Convert.ToBase64String(hashmessage);

            }
            return Temp;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }



        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }



        public static string sha256(string randomString)
        {
            using (var sha256 = SHA256.Create())
            {
                // Send a sample text to hash.  
                var hashedBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(randomString));
                // Get the hashed string.  
                var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                // Print the string.   
                return hash.ToString();
            }


            //var crypt = new System.Security.Cryptography.SHA256Managed();
            //var hash = new System.Text.StringBuilder();
            //byte[] crypto = crypt.ComputeHash(System.Text.Encoding.UTF8.GetBytes(randomString));
            //foreach (byte theByte in crypto)
            //{
            //    hash.Append(theByte.ToString("x2"));
            //}
            // return hash.ToString();
        }

        public static string HmacSHA256(string key, string data)
        {
            string hash;
            ASCIIEncoding encoder = new ASCIIEncoding();
            Byte[] code = encoder.GetBytes(key);
            using (HMACSHA256 hmac = new HMACSHA256(code))
            {
                Byte[] hmBytes = hmac.ComputeHash(encoder.GetBytes(data));
                hash = ToHexString(hmBytes);
            }
            return hash;
        }

        public static string ToHexString(byte[] array)
        {
            StringBuilder hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }

        public static string GetSP(string SP, string paramSP, string paramJson)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(conn2))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand(SP, cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add(paramSP, SqlDbType.NVarChar).Value = paramJson;


                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore,
                                TypeNameHandling = TypeNameHandling.All
                            };
                            //string serializedString = JsonConvert.SerializeObject(s, settings);
                            return s.ToString();

                        }
                    }
                }
                return @"{}"; ;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        public static string PostSP(string SP, string paramsSP, string paramJson)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(conn2))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand(SP, cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add(paramsSP, SqlDbType.NVarChar).Value = paramJson;

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            var s = reader.Value.ToString();
                            return s.ToString();
                        }
                    }
                }
                return @"{}"; 
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static string PostLog(string SP, string paramsSP, string paramJson)
        {
            try
            {
                LogModel lm = new LogModel()
                {
                    Log_Apps = "CintaID NC21",
                    Log_Descriptions = paramJson,
                    Log_Modules = SP,
                    Username = "System"
                };

                paramJson = JsonConvert.SerializeObject(lm);

                using (SqlConnection cnn = new SqlConnection(conn2))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand(SP, cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add(paramsSP, SqlDbType.NVarChar).Value = paramJson;

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            var s = reader.Value.ToString();
                            return s.ToString();
                        }
                    }
                }
                return @"{}"; ;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


        public static string GetSPxml(string SP, string paramSP, string paramJson)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(conn2))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand(SP, cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add(paramSP, SqlDbType.NVarChar).Value = paramJson;


                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore,
                                TypeNameHandling = TypeNameHandling.All
                            };
                            //string serializedString = JsonConvert.SerializeObject(s, settings);
                            return s.ToString();

                        }
                    }
                }
                return @"{}"; ;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }


    }
}
