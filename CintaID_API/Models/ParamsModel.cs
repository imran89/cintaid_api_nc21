﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class ParamsHeaderModel
    {
        public string Username { get; set; }
        public string Amount { get; set; }
        public string Saldo { get; set; }
    }

    public class ParamsBodyModel
    {
        public string TransactionID { get; set; }
        public string ReferenceID { get; set; }
        public string Amount { get; set; }
        public string SourceAccountNumber { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
    }
}
