﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class BCA_Respons_Model
    {
        public string TransactionID { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ReferenceID { get; set; }
        public string PPUNumber { get; set; }
    }
}
