﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Xml;

namespace CintaID_API.Models.Common
{
    public static class CommonTFBCA_Model
    {
        public static String UrlEncode(this String stringToEncode)
        {
            return System.Web.HttpUtility.UrlEncode(stringToEncode);
        }

        //public static string CS = ConfigurationManager.ConnectionStrings["CINTAIDCS"].ConnectionString;
        public static string GetFunction(string partner, string code, bool environment)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn2))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_Partner_Cred", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add("@Partner", SqlDbType.NVarChar).Value = partner;
                    cmd.Parameters.Add("@Code", SqlDbType.NVarChar).Value = code;
                    cmd.Parameters.Add("@Environment", SqlDbType.NVarChar).Value = environment;

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            return reader.Value.ToString();
                        }
                        return "Please, be Nice and contact thomas.benny@cinta.id for holes.";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();

            }

        }
    }
}
