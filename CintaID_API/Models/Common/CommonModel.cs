﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace CintaID_API.Models.Common
{
    public static class CommonModel
    {
        public static string ReadCintaID(string SP, string paramJson, string conn)
        {
            try
            {
                string resultJson = "";
                using (SqlConnection cnn = new SqlConnection(conn))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand(SP, cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add("@paramJson", SqlDbType.NVarChar).Value = paramJson;

                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            resultJson = s.ToString();
                            return resultJson;
                        }
                    }
                }
                return resultJson;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

    }
}
