﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class SendEmailModel
    {
        public string Customer_Name { get; set; }
        public int PYM_ID { get; set; }
        public string PYM_BF_TRANS_CODE { get; set; }
        public int PYM_SALES_ID { get; set; }
        public decimal PYM_SALES_AMOUNT { get; set; }
        public decimal PYM_UNIQUE_NO { get; set; }
        public decimal PYM_UNIQUE_AMOUNT { get; set; }
        public string PYM_REFF_NO { get; set; }
        public int PYM_TYPE_ID { get; set; }
        public int PYM_ORDER_TYPE_ID { get; set; }
        public int PYM_METHOD_ID { get; set; }
        public decimal PYM_AMOUNT { get; set; }
        public string PYM_HP_NO { get; set; }
        public string PYM_DESC { get; set; }
        public string PYM_CATEGORY { get; set; }
        public string Tipe_Transaksi { get; set; }
        public string PYM_PAID_STATUS { get; set; }
        public bool PYM_STATUS_MC { get; set; }
        public DateTime PYM_SEND_DATE_MC { get; set; }
        public bool PYM_ISPAID { get; set; }
        public bool PYM_ISDELETE { get; set; }
        public DateTime PYM_INSERTON { get; set; }
        public int PYM_INSERTBY { get; set; }
        public DateTime PYM_UPDATEON { get; set; }
        public int PYM_UPDATEBY { get; set; }
        public int PYM_CONFIRM_BY { get; set; }
        public DateTime PYM_CONFIRM_ON { get; set; }
        public DateTime PYM_LAST_ON { get; set; }

        public int TransID { get; set; }
        public string TransCode { get; set; }
        public int Sales_Merchant_ID { get; set; }
        public int Sales_Promo_ID { get; set; }
        public string Sales_Delivery_ID { get; set; }
        public DateTime Sales_Assignment_Time { get; set; }
        public decimal Sales_Delivery_Price { get; set; }
        public decimal Sales_Promotion_Amout { get; set; }
        public decimal Sales_Gross_Amount { get; set; }
        public decimal Sales_PPN_Amount { get; set; }
        public decimal Sales_Nett_amount { get; set; }
        public int Sales_Total_Qty { get; set; }
        public decimal Sales_Total_Amount { get; set; }
        public string Sales_Desc { get; set; }
        public bool Sales_isPaid { get; set; }
        public bool Sales_isClosed { get; set; }
        public int Sales_IsApproval { get; set; }
        public string Sales_App { get; set; }
        public string Sales_Address { get; set; }
        public bool Sales_IsDelete { get; set; }
        public DateTime Sales_InsertOn { get; set; }
        public decimal Sales_InsertLong { get; set; }
        public decimal Sales_InsertLad { get; set; }
        public DateTime Sales_UpdateOn { get; set; }

        public string PaymentType_Code { get; set; }
        public string PaymentType_Name { get; set; }
        public string PaymentType_Desc { get; set; }
        public string PaymentType_Pict { get; set; }

        public string Merchant_Name { get; set; }
        public string Merchant_Image1 { get; set; }
        public string Merchant_Address { get; set; }
        public string Merchant_HP { get; set; }
        public string Merchant_Email { get; set; }
        public decimal Merchant_Latitude { get; set; }
        public decimal Merchant_Longitude { get; set; }
        public List<_Details> Detail { get; set; }
    }
    public class _Details
    {
        public int idx { get; set; }
        public int TransID { get; set; }
        public int Sales_No { get; set; }
        public int Sales_Item_ID { get; set; }
        public string Sales_Item_Name { get; set; }
        public int Sales_Qty { get; set; }
        public string Sales_UOM_Code { get; set; }
        public decimal Sales_Price { get; set; }
        public decimal Sales_Discount { get; set; }
        public decimal Sales_LineTotal { get; set; }
    }
}
