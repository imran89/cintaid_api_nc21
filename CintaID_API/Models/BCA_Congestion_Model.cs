﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class BCA_Congestion_Model
    {
        public string CorporateID { get; set; }
        public string SourceAccountNumber { get; set; }
        public string TransactionID { get; set; }
        public string TransactionDate { get; set; }
        public string ReferenceID { get; set; }
        public string CurrencyCode { get; set; }
        public string Amount { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string Remark1 { get; set; }
        public string Remark2 { get; set; }
    }


}
