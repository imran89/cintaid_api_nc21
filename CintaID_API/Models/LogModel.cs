﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class LogModel
    {
        public string Log_Descriptions { get; set; }
        public string Log_Apps { get; set; }
        public string Log_Modules { get; set; }
        public string Log_Version { get; set; }
        public string Username { get; set; }

    }
}
