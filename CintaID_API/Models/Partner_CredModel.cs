﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CintaID_API.Models
{
    public class Partner_CredModel
    {
        public int Cred_ID { set; get; }
        public string Cred_Partner { set; get; }
        public string Cread_CorpID { set; get; }
        public string Cred_ClientID { set; get; }
        public string Cred_Secret { set; get; }
        public string Cred_NoReff { set; get; }
        public string Cred_ApiKey { set; get; }
        public string Cred_SecretKey { set; get; }
        public string Cred_Environment { set; get; }
        public int Fnc_Id { set; get; }
        public string Fnc_Code { set; get; }
        public string Fnc_Name { set; get; }
        public string Fnc_Desc { set; get; }
        public string Fnc_Partner { set; get; }
        public string Fnc_Url { set; get; }
        public string Fnc_Uri { set; get; }
        public string Fnc_Method { set; get; }
    }

    public class Token
    {
        public string access_token { set; get; }
        public string token_type { set; get; }
        public int expires_in { set; get; }
        public string scope { set; get; }
    }


}
