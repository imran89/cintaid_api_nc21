﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Login", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@password", SqlDbType.NVarChar).Value = Password;
                        

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Module, string App)
        {
            try
            {
                SqlDataReader rdr = null;
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_GET_EMAIL", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Module", SqlDbType.NVarChar).Value = Module;
                        cmd.Parameters.Add("@App", SqlDbType.NVarChar).Value = App;
                        cmd.Parameters.Add("@Username", SqlDbType.NVarChar).Value = Username;
                        cmd.Parameters.Add("@dt", SqlDbType.NVarChar).Value = dt;
                        cmd.Parameters.Add("@keys", SqlDbType.NVarChar).Value = keys;


                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                //return s.ToString();
                                while (rdr.Read())
                                {
                                    CintaID_API.MessageServices.SendEmailAsync(Username, rdr["title"].ToString(), rdr["bodyHTML"].ToString(), rdr["bodyText"].ToString());

                                }
                                return @"{""status"":""email has been sent.""}";
                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Parameter}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Parameter)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    if (Parameter == "Checkusername")
                    {
                        using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                        {
                            cnn.Open();
                            SqlCommand cmd = new SqlCommand("SP_Get_Check_ID", cnn)
                            {
                                CommandTimeout = 30,
                                CommandType = CommandType.StoredProcedure
                            };

                            cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = Username;


                            using (XmlReader reader = cmd.ExecuteXmlReader())
                            {
                                while (reader.Read())
                                {
                                    string s = reader.Value.ToString();
                                    var settings = new JsonSerializerSettings
                                    {
                                        NullValueHandling = NullValueHandling.Ignore,
                                        MissingMemberHandling = MissingMemberHandling.Ignore
                                    };

                                    return s.ToString();

                                }
                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("SP_Get_Login_Json", "@M_UserJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        

    }
}