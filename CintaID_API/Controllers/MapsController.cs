﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapsController : Controller
    {
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Selection}/{Search}/{Long}/{Lad}/{Rad}/{Tipe}/{UserID}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Selection, string Search, double Long, double Lad, int Rad, string Tipe, int UserID)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_MapMerchant", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Selection", SqlDbType.NVarChar).Value = Selection;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Long", SqlDbType.Decimal).Value = Long;
                        cmd.Parameters.Add("@Lad", SqlDbType.Decimal).Value = Lad;
                        cmd.Parameters.Add("@Rad", SqlDbType.NVarChar).Value = Rad;
                        cmd.Parameters.Add("@Tipe", SqlDbType.NVarChar).Value = Tipe;
                        cmd.Parameters.Add("@UserID", SqlDbType.NVarChar).Value = UserID;

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";



                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

       

    }
}