﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShareController : Controller
    {
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string deCode = CintaID_API.CLS_FUNCT.Base64Decode(paramJson);

                    string jsonPaid = "";
                    return CLS_FUNCT.GetSP("SP_GetShare_Search", "@paramJson", deCode);

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_Tr_Shares_Json", "@Tr_SharesJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> DeleteUpdate(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Del_Tr_shares", "@Tr_SharesJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }


        }

    }
}