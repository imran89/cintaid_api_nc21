﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CintaID_API.Models;
using CintaID_API.Models.Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisbursementBCAController : Controller
    {
        public static string getdatetime;
        public static string Signature;

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string deCode = CintaID_API.CLS_FUNCT.Base64Decode(paramJson);

                    string jsonPaid = "";
                    return CLS_FUNCT.GetSP("SP_Get_M_Advertisement", "@paramJson", deCode);

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        public static string token;
        public static string Token()
        {

            try
            {
                Partner_CredModel credjson;
                string Func_Cred = CommonTFBCA_Model.GetFunction("BCA01", "BCA_TOKEN", true);
                credjson = JsonConvert.DeserializeObject<Partner_CredModel>(Func_Cred.ToString());
                var httpClient = new HttpClient();
                var creds = string.Format("{0}:{1}", credjson.Cred_ClientID, credjson.Cred_Secret);
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(creds)));
                httpClient.DefaultRequestHeaders.Add("Authorization", basicAuth);
                var post = httpClient.PostAsync(credjson.Fnc_Url + credjson.Fnc_Uri,
                    //sudah digabung di url+ cred.Token,
                    new FormUrlEncodedContent(new Dictionary<string, string>
                        {   { "grant_type", "client_credentials" }  }));
                post.Wait();
                var contents = post.Result.Content.ReadAsStringAsync().Result;
                Token json = JsonConvert.DeserializeObject<Token>(contents.ToString());
                token = json.access_token.ToString();
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", "Token : " + contents.ToString());
                //INS_Intruders(token.ToString());
                return json.access_token.ToString();

            }
            catch (Exception ex)
            {
                token = @"{""error:""" + ex.Message.ToString() + @"""}";
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", "Token Error : " + token.ToString());
                return @"{""error:""" + ex.Message.ToString() + @"""}";
            }

        }

        Partner_CredModel obj = new Partner_CredModel();
        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                ParamsHeaderModel hdrMdl = new ParamsHeaderModel();
                BCA_Respons_Model rspMdl = new BCA_Respons_Model();
                ParamsBodyModel bdyMdl = new ParamsBodyModel();
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                paramJson = CLS_FUNCT.Base64Decode(paramJson);
                hdrMdl = JsonConvert.DeserializeObject<ParamsHeaderModel>(paramJson, settings);
                string paramBodyJson = new StreamReader(Request.Body).ReadToEnd();
                bdyMdl = JsonConvert.DeserializeObject<ParamsBodyModel>(paramBodyJson, settings);
                if (Convert.ToDecimal(hdrMdl.Saldo) >= (Convert.ToDecimal(bdyMdl.Amount) + 3500))
                {

                    Token();
                    string StringToSign;
                    getdatetime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
                    string urlSign;
                    if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                    {
                        string Func_Cred = CommonTFBCA_Model.GetFunction("BCA01", "BCA_TRANSFER_OTHER", true);
                        obj = JsonConvert.DeserializeObject<Partner_CredModel>(Func_Cred.ToString());


                        var httpClient = new HttpClient();
                        var creds = string.Format("{0}:{1}", obj.Cred_ClientID, obj.Cred_Secret);
                        var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(creds)));

                        urlSign = obj.Fnc_Uri.Replace("@norek", CommonTFBCA_Model.UrlEncode(obj.Cred_NoReff.ToString()));
                        StringToSign = "POST" + ":" + urlSign + ":" + token + ":" + CLS_FUNCT.sha256(paramBodyJson).ToLower() + ":" + getdatetime;
                        Signature = CLS_FUNCT.HmacSHA256(obj.Cred_SecretKey, StringToSign);

                        httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                        httpClient.DefaultRequestHeaders.Add("HTTPMethod", "POST");
                        httpClient.DefaultRequestHeaders.Add("X-BCA-Signature", Signature);
                        httpClient.DefaultRequestHeaders.Add("X-BCA-Key", obj.Cred_ApiKey);
                        httpClient.DefaultRequestHeaders.Add("X-BCA-Timestamp", getdatetime);
                        httpClient.DefaultRequestHeaders.Add("ChannelID", "95051");
                        httpClient.DefaultRequestHeaders.Add("CredentialID", obj.Cread_CorpID);// "h2hauto008");            //httpClient.DefaultRequestHeaders.Add("AccessToken", token);
                                                                                               //httpClient.DefaultRequestHeaders.Add("APISecret", CLS_Static.SecretAPI);
                        var content = new StringContent(paramBodyJson, Encoding.UTF8, "application/json");
                        var post = httpClient.PostAsync(obj.Fnc_Url + CLS_FUNCT.URITransDom, content);
                        post.Wait();
                        var contents = post.Result.Content.ReadAsStringAsync().Result;
                        string status = contents.ToString();

                        rspMdl = JsonConvert.DeserializeObject<BCA_Respons_Model>(status, settings);

                        if (rspMdl.TransactionID == bdyMdl.TransactionID)
                        {
                            string resultPost = CLS_FUNCT.PostSP("UPDT_Saldo_Mutation", "@paramJson", paramJson);
                            CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", status.ToString() + ' ' + resultPost.ToString());
                            return status.ToString();
                        }
                        else
                        {
                            CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", status.ToString());
                            return status.ToString();
                        }

                        //string hsl = post.Result.Content.ReadAsStringAsync().Status.ToString();
                        //CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", "Result TF to Other : " + status.ToString());
                        //return contents.ToString();
                    }
                }
                else
                {
                    return @"{""Message"":""Saldo Anda Tidak Mencukupi""}";
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


    }
}