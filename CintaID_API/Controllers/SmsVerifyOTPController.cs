﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmsVerifyOTPController : Controller
    {

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    var httpClient = new HttpClient();

                    httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Y2ludGEuaWQ6MjVmZjkxMGM5MGJkYmE4ZjNlZmI5NmZkNTI1ZTc4Mzg=");
                    httpClient.DefaultRequestHeaders.Add("HTTPMethod", "POST");
                    var content = new StringContent(paramJson, Encoding.UTF8, "application/json");
                    var post = httpClient.PostAsync("https://gateway.citcall.com/v3/verify", content);
                    post.Wait();
                    var contents = post.Result.Content.ReadAsStringAsync().Result;

                    //await post.Result.Content.ReadAsStringAsync();
                    return contents.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        


    }
}