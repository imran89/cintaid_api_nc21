﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CintaID_API.Models;
using CintaID_API.Models.Common;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisbursCongestionBCAController : Controller
    {
        public static string getdatetime;
        public static string Signature;

        public static string token;
        public static string Token()
        {

            try
            {
                Partner_CredModel credjson;
                string Func_Cred = CommonTFBCA_Model.GetFunction("BCA01", "BCA_TOKEN", true);
                credjson = JsonConvert.DeserializeObject<Partner_CredModel>(Func_Cred.ToString());
                var httpClient = new HttpClient();
                var creds = string.Format("{0}:{1}", credjson.Cred_ClientID, credjson.Cred_Secret);
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(creds)));
                httpClient.DefaultRequestHeaders.Add("Authorization", basicAuth);
                var post = httpClient.PostAsync(credjson.Fnc_Url + credjson.Fnc_Uri,
                    //sudah digabung di url+ cred.Token,
                    new FormUrlEncodedContent(new Dictionary<string, string>
                        {   { "grant_type", "client_credentials" }  }));
                post.Wait();
                var contents = post.Result.Content.ReadAsStringAsync().Result;
                Token json = JsonConvert.DeserializeObject<Token>(contents.ToString());
                token = json.access_token.ToString();
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", "Token : " + token.ToString());
                return json.access_token.ToString();

            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", "Token : " + ex.ToString());

                return @"{""error:""" + ex.Message.ToString() + @"""}";
            }

        }

        public static string sign_transfer(string Payload)
        {
            try
            {
                token = Token();
                //token = "Oa0cwUXbJr5QLbG8NUrSc4yyLxPVcEORnP3qUA0WDppbxJHZZAL1Ao";
                getdatetime = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fffzzz");
                //getdatetime = "2019-05-08T13:28:53.173+07:00";
                string StringToSign;
                Partner_CredModel credjson;

                string Func_Cred = CommonTFBCA_Model.GetFunction("BCA01", "BCA_TRANSFER", true);
                credjson = JsonConvert.DeserializeObject<Partner_CredModel>(Func_Cred.ToString());
                StringToSign = credjson.Fnc_Method + ":" + credjson.Fnc_Uri + ":" + token + ":" + CLS_FUNCT.sha256(Payload.Trim()).ToLower() + ":" + getdatetime;
                Signature = CLS_FUNCT.HmacSHA256(credjson.Cred_SecretKey, StringToSign);
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", "sign_transfer : " + Signature.ToString());
                return Signature.ToString();
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", ex.ToString());
                return ex.Message.ToString();
            }
        }

        public static string Proc_Transfer(string payload, Partner_CredModel credjson)
        {
            try
            {
                sign_transfer(payload);

                var httpClient = new HttpClient();
                var creds = string.Format("{0}:{1}", credjson.Cred_ClientID, credjson.Cred_Secret);
                var basicAuth = string.Format("Basic {0}", Convert.ToBase64String(Encoding.UTF8.GetBytes(creds)));
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
                httpClient.DefaultRequestHeaders.Add("HTTPMethod", credjson.Fnc_Method);
                httpClient.DefaultRequestHeaders.Add("X-BCA-Signature", Signature);
                httpClient.DefaultRequestHeaders.Add("X-BCA-Key", credjson.Cred_ApiKey);
                httpClient.DefaultRequestHeaders.Add("X-BCA-Timestamp", getdatetime);
                //httpClient.DefaultRequestHeaders.Add("Timestamp", getdatetime);
                //httpClient.DefaultRequestHeaders.Add("URI", CLS_Static.URITransfer);
                //httpClient.DefaultRequestHeaders.Add("AccessToken", token);
                //httpClient.DefaultRequestHeaders.Add("APISecret", CLS_Static.SecretAPI);
                var content = new StringContent(payload, Encoding.UTF8, "application/json");
                var post = httpClient.PostAsync(credjson.Fnc_Url + credjson.Fnc_Uri, content);
                post.Wait();
                var contents = post.Result.Content.ReadAsStringAsync().Result;
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", contents.ToString());
                return contents.ToString();
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", ex.ToString());
                return ex.Message.ToString();
            }

        }


        Partner_CredModel obj = new Partner_CredModel();
        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys, string paramJson)
        {
            try
            {
                string paramBodyJson = new StreamReader(Request.Body).ReadToEnd();
                BCA_Respons_Model rspMdl = new BCA_Respons_Model();
                ParamsBodyModel bdyMdl = new ParamsBodyModel();
                ParamsHeaderModel hdrMdl = new ParamsHeaderModel();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    var settings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        MissingMemberHandling = MissingMemberHandling.Ignore
                    };

                    paramJson = CLS_FUNCT.Base64Decode(paramJson);
                    hdrMdl = JsonConvert.DeserializeObject<ParamsHeaderModel>(paramJson, settings);

                    if (Convert.ToDecimal(hdrMdl.Saldo) >= Convert.ToDecimal(bdyMdl.Amount))
                    {
                        string status = "";
                        Partner_CredModel credjson;
                        BCA_Congestion_Model vInParams = new BCA_Congestion_Model();

                        string Func_Cred = CommonTFBCA_Model.GetFunction("BCA01", "BCA_TRANSFER", true);
                        credjson = JsonConvert.DeserializeObject<Partner_CredModel>(Func_Cred.ToString());
                        vInParams = JsonConvert.DeserializeObject<BCA_Congestion_Model>(paramBodyJson.ToString());

                        vInParams.CorporateID = credjson.Cread_CorpID.ToString();

                        paramBodyJson = JsonConvert.SerializeObject(vInParams);

                        status = Proc_Transfer(paramBodyJson, credjson);
                        //INS_Intruders(contents.ToString());

                        rspMdl = JsonConvert.DeserializeObject<BCA_Respons_Model>(status, settings);
                        bdyMdl = JsonConvert.DeserializeObject<ParamsBodyModel>(paramBodyJson, settings);

                        if (rspMdl.TransactionID == bdyMdl.TransactionID)
                        {
                            string resultPost = CLS_FUNCT.PostSP("UPDT_Saldo_Mutation", "@paramJson", paramJson);
                            CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", status.ToString() + ' ' + resultPost.ToString());
                            return status.ToString();
                        }
                        else
                        {
                            return status.ToString();
                        }
                    }
                    else
                    {
                        return @"{""Message"":""Saldo Anda Tidak Mencukupi""}";
                    }
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "@Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


    }
}