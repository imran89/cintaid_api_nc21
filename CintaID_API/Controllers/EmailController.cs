﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : Controller
    {
        // GET api/Search
        [HttpGet]
        public async Task<string> Get()
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_LoginALL", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };


                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            return s.ToString();

                        }
                    }
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson",  ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        // GET api/Search
        [HttpGet("{username}/{password}/{key}")]
        public async Task<string> Get(string username, string password, string key)
        {
            try
            {
                if (key == CintaID_API.CLS_FUNCT.Base64Encode(username.ToString()))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Login", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@username", SqlDbType.NVarChar).Value = username;
                        cmd.Parameters.Add("@password", SqlDbType.NVarChar).Value = password;


                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        //private readonly IInfoDeviceServices _services;
        [HttpPost()]
        public ActionResult<string> Add()
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                string replayJson = CLS_FUNCT.PostSP("SP_Login", "@loginJson", paramJson);
                return replayJson.ToString();
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


    }
}