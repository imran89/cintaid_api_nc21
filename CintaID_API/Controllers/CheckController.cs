﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckController : Controller
    {
        // GET api/Search
        [HttpGet]
        public async Task<string> Get()
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                {
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("SP_get_CheckPoints", cnn)
                    {
                        CommandTimeout = 30,
                        CommandType = CommandType.StoredProcedure
                    };

                    cmd.Parameters.Add("@idx", SqlDbType.NVarChar).Value = 0;


                    using (XmlReader reader = cmd.ExecuteXmlReader())
                    {
                        while (reader.Read())
                        {
                            string s = reader.Value.ToString();
                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };

                            return s.ToString();

                        }
                    }
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{idx}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, double idx)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_get_CheckPoints", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@idx", SqlDbType.NVarChar).Value = idx;


                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_Tr_checkPoints_Json", "@Tr_checkPointsJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // DELETE api/Todo/5
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{delete}")]
        public ActionResult<string> DeleteUpdate(string Username, string Password, string dt, string keys, string delete)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Del_Tr_checkPoints", "@Tr_checkPointsJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }


        }

    }
}