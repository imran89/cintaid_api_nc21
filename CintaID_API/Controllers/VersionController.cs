﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VersionController : Controller
    {
        // GET api/Search
        [HttpGet]
        public async Task<string> Get()
        {
            //return @"{""Version_Code"":""NC.21.V.1.0.2."",""Version_Get"":""SendEmail Display Product""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.3."",""Version_Get"":""OTP Verify""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.4."",""Version_Get"":""Disbursement BCA""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.5."",""Version_Get"":""Disbursement Congestion BCA""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.6."",""Version_Get"":""Check Login By Number or Email BU_GitLab2""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.7."",""Version_Get"":""GET Promo (Promo Detail)3""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.8."",""Version_Get"":""GET Promo Used""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.9."",""Version_Get"":""Feedback""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.10."",""Version_Get"":""Inbox""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.13."",""Version_Get"":""Revisi SendEmail3""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.14."",""Version_Get"":""Revisi Transfer""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.15."",""Version_Get"":""Revisi Transfer Decode Param""}";
            //return @"{""Version_Code"":""NC.21.V.1.0.16."",""Version_Get"":""Revisi Transfer Other Charge""}";
            return @"{""Version_Code"":""NC.21.V.1.0.17."",""Version_Get"":""Revisi Send Email""}";
        }

       

    }
}