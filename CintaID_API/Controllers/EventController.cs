﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CintaID_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : Controller
    {
        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Id}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, string Id, double Customer_Id)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_Event_ID", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@id", SqlDbType.NVarChar).Value = Id;
                        

                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        // GET api/Search
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Id}/{Search}/{Long}/{Lad}/{Radius}")]
        public async Task<string> Get(string Username, string Password, string dt, string keys, int Id, string Search, double Long, double Lad, double Radius)
        {
            try
            {
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    using (SqlConnection cnn = new SqlConnection(CLS_FUNCT.conn))
                    {
                        cnn.Open();
                        SqlCommand cmd = new SqlCommand("SP_Get_Event_Search", cnn)
                        {
                            CommandTimeout = 30,
                            CommandType = CommandType.StoredProcedure
                        };

                        cmd.Parameters.Add("@Id", SqlDbType.NVarChar).Value = Id;
                        cmd.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                        cmd.Parameters.Add("@Long", SqlDbType.NVarChar).Value = Long;
                        cmd.Parameters.Add("@Lad", SqlDbType.NVarChar).Value = Lad;
                        cmd.Parameters.Add("@Radius", SqlDbType.NVarChar).Value = Radius;


                        using (XmlReader reader = cmd.ExecuteXmlReader())
                        {
                            while (reader.Read())
                            {
                                string s = reader.Value.ToString();
                                var settings = new JsonSerializerSettings
                                {
                                    NullValueHandling = NullValueHandling.Ignore,
                                    MissingMemberHandling = MissingMemberHandling.Ignore
                                };

                                return s.ToString();

                            }
                        }
                    }
                    return @"{}";

                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }


        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public ActionResult<string> Add(string Username, string Password, string dt, string keys)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_Tr_Events_Json", "@Tr_EventsJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{Detail}")]
        public ActionResult<string> Add2(string Username, string Password, string dt, string keys, string Detail)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_Tr_EventsDetail_Json", "@Tr_EventsDetailJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }

        //private readonly IInfoDeviceServices _services;
        [HttpPost("{Username}/{Password}/{dt}/{keys}/{Detail}/{Join}/")]
        public ActionResult<string> Add3(string Username, string Password, string dt, string keys, string Detail, string Join)
        {
            try
            {
                string paramJson = new StreamReader(Request.Body).ReadToEnd();
                if (keys == CintaID_API.CLS_FUNCT.Base64Encode(Username + CintaID_API.CLS_FUNCT.cintaidkeys + dt))
                {
                    string replayJson = CLS_FUNCT.PostSP("Ins_EventJoin_Json", "@EventJoinJson", paramJson);
                    return replayJson.ToString();
                }
                return @"{}";
            }
            catch (Exception ex)
            {
                CLS_FUNCT.PostLog("SP_INS_LOGS", "Tr_LogsJson", keys.ToString() + ", Error : " + ex.ToString());
                return "Error : " + ex.ToString();
            }
        }



    }
}